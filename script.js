const ourServicesTabs = document.querySelectorAll("li.services");
const tabsContent = document.querySelectorAll("div.service-content");

ourServicesTabs.forEach((elem) => {
  let tabElem = elem;
  elem.addEventListener("click", () => {
    ourServicesTabs.forEach((elem) => {
      elem.classList.remove("service-active");
    });
    elem.classList.add("service-active");

    tabsContent.forEach((elem) => {
      if (elem.dataset.id != tabElem.dataset.tab) {
        elem.classList.add("hidden");
      } else {
        elem.classList.remove("hidden");
      }

    })
  })
})



const ourWorksContent = document.querySelector(".our-amazing-work__images-wrapper");

const categoriesList = document.querySelector(".our-amazing-work__list");
const ourWorksCategories = document.querySelectorAll(".work-category");
const loadMoreBtn = document.querySelector(".our-amazing-work .load-more");


const imgDataBase = {
  'graphic-design': {
    images: [
      './img/Our Amazing Work Section/graphic-design/graphic-design-04.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-05.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-06.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-07.webp',
      './img/Our Amazing Work Section/graphic-design/graphic-design-08.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-09.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-10.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-11.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-12.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-13.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-14.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-15.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-16.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-17.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-18.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-19.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-20.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-21.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-22.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-23.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-24.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-25.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-26.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-27.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-28.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-29.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-30.webp',
      './img/Our Amazing Work Section/graphic-design/graphic-design-31.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-32.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-33.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-34.jpg',
      './img/Our Amazing Work Section/graphic-design/graphic-design-35.webp',
      './img/Our Amazing Work Section/graphic-design/graphic-design-36.jpg',
    ],
  },
  'web-design': {
    images: [
      './img/Our Amazing Work Section/web-design/web-design-04.jpg',
      './img/Our Amazing Work Section/web-design/web-design-05.jpg',
      './img/Our Amazing Work Section/web-design/web-design-06.jpg',
      './img/Our Amazing Work Section/web-design/web-design-07.jpg',
      './img/Our Amazing Work Section/web-design/web-design-08.jpg',
      './img/Our Amazing Work Section/web-design/web-design-09.jpg',
      './img/Our Amazing Work Section/web-design/web-design-10.jpg',
      './img/Our Amazing Work Section/web-design/web-design-11.jpg',
      './img/Our Amazing Work Section/web-design/web-design-12.jpg',
      './img/Our Amazing Work Section/web-design/web-design-13.jpg',
      './img/Our Amazing Work Section/web-design/web-design-14.jpg',
      './img/Our Amazing Work Section/web-design/web-design-15.jpg',
      './img/Our Amazing Work Section/web-design/web-design-16.jpg',
      './img/Our Amazing Work Section/web-design/web-design-17.jpg',
      './img/Our Amazing Work Section/web-design/web-design-18.jpg',
      './img/Our Amazing Work Section/web-design/web-design-19.jpg',
      './img/Our Amazing Work Section/web-design/web-design-20.jpg',
      './img/Our Amazing Work Section/web-design/web-design-21.jpg',
      './img/Our Amazing Work Section/web-design/web-design-22.jpg',
      './img/Our Amazing Work Section/web-design/web-design-23.jpg',
      './img/Our Amazing Work Section/web-design/web-design-24.jpg',
      './img/Our Amazing Work Section/web-design/web-design-25.jpg',
      './img/Our Amazing Work Section/web-design/web-design-26.jpg',
      './img/Our Amazing Work Section/web-design/web-design-27.jpg',
      './img/Our Amazing Work Section/web-design/web-design-28.jpg',
      './img/Our Amazing Work Section/web-design/web-design-29.jpg',
      './img/Our Amazing Work Section/web-design/web-design-30.jpg',
      './img/Our Amazing Work Section/web-design/web-design-31.webp',
      './img/Our Amazing Work Section/web-design/web-design-32.jpg',
      './img/Our Amazing Work Section/web-design/web-design-33.jpg',
      './img/Our Amazing Work Section/web-design/web-design-34.webp',
      './img/Our Amazing Work Section/web-design/web-design-35.webp',
      './img/Our Amazing Work Section/web-design/web-design-36.webp',
    ],
  },
  'landing-pages': {
    images: [
      './img/Our Amazing Work Section/landing-page/landing-page-04.png',
      './img/Our Amazing Work Section/landing-page/landing-page-05.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-06.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-07.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-08.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-09.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-10.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-11.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-12.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-13.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-14.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-15.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-16.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-17.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-18.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-19.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-20.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-21.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-22.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-23.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-24.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-25.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-26.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-27.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-28.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-29.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-30.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-31.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-32.jpg',
      './img/Our Amazing Work Section/landing-page/landing-page-33.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-34.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-35.webp',
      './img/Our Amazing Work Section/landing-page/landing-page-36.webp',
    ],
  },
  'wordpress': {
    images: [
      './img/Our Amazing Work Section/wordpress/wordpress-04.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-05.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-06.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-07.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-08.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-09.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-10.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-11.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-12.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-13.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-14.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-15.webp',
      './img/Our Amazing Work Section/wordpress/wordpress-16.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-17.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-18.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-19.webp',
      './img/Our Amazing Work Section/wordpress/wordpress-20.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-21.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-22.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-23.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-24.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-25.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-26.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-27.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-28.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-29.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-30.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-31.jpg',
      './img/Our Amazing Work Section/wordpress/wordpress-32.webp',
      './img/Our Amazing Work Section/wordpress/wordpress-33.webp',
      './img/Our Amazing Work Section/wordpress/wordpress-34.webp',
      './img/Our Amazing Work Section/wordpress/wordpress-35.webp',
      './img/Our Amazing Work Section/wordpress/wordpress-36.jpg',
    ],
  },
};

let totalImgNumber = 0;
let imgCount = 0;
let graphDesImgCount = 0;
let webDesImgCount = 0;
let landPageImgCount = 0;
let wordpressImgCount = 0;

let totalImgLoaded = 0;
let allImgLoaded = false;

for (const category in imgDataBase) {
  if (imgDataBase.hasOwnProperty(category)) {
    totalImgNumber += imgDataBase[category].images.length;
  }
}


const categoryFilter = (categoryKey) => {
  ourWorksContent.querySelectorAll('.flip-card').forEach(item => {
    if (categoryKey !== item.dataset.id && categoryKey !== 'all') {
      item.style.display = 'none';
    } else {
      item.style.display = 'block';
    }
  });
};


const setActiveTab = (tab) => {
  categoriesList.querySelectorAll('.work-category').forEach(elem => {
      elem.classList.remove('active-category');
  });
  tab.classList.add('active-category');
};

// if (activeCategory === 'all') {
const createCards = () => {
  const activeCategory = categoriesList.querySelector('.active-category').dataset.tab;
  let categoryImages;
  if (activeCategory === 'all') {
    for (const category in imgDataBase) {
      categoryImages = imgDataBase[category].images.slice(imgCount, imgCount + 3);
      categoryImages.forEach(imageSrc => {
        ourWorksContent.insertAdjacentHTML('beforeend', `
                        <div data-id="${category}" class="flip-card">
                            <div class="flip-card-front">
                                <img src="${imageSrc}"
                                    alt="graphic-design-illustration">
                            </div>
                            <div class="flip-card-back">
                                <svg xmlns="http://www.w3.org/2000/svg" width="88" height="43" viewBox="0 0 88 43"
                                    fill="none">
                                    <g clip-path="url(#clip0_2143_233)">
                                        <rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z"
                                            fill="#1FDAB5" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M66.5973 1.99795C77.8653 1.99795 86.9999 10.9523 86.9999 21.9979C86.9999 33.0432 77.8653 41.9979 66.5973 41.9979C55.3292 41.9979 46.1946 33.0432 46.1946 21.9979C46.1946 10.9523 55.3292 1.99795 66.5973 1.99795Z"
                                            fill="#18CFAB" />
                                        <rect x="60" y="17" width="12" height="11" fill="white" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_2143_233">
                                            <rect width="88" height="43" fill="white" />
                                        </clipPath>
                                    </defs>
                                </svg>
                                <p class="back-title text-green">Creative Design</p>
                                <p class="back-subtitle">Web Design</p>
                            </div>
                        </div>
              `);
      });
      totalImgLoaded += 3;
    }
    imgCount += 3;
    // categoryImgCount += 3;

    if (totalImgLoaded >= totalImgNumber) {
      allImgLoaded = true;
      loadMoreBtn.style.display = 'none';
      return;
    }

  
  } else if(activeCategory !== "all") {
    console.log(activeCategory);
    console.log(imgDataBase[activeCategory].images.slice(imgCount, imgCount + 3));
    categoryImages = imgDataBase[activeCategory].images.slice(imgCount, imgCount + 3); 
    categoryImages.forEach(imageSrc => {
      ourWorksContent.insertAdjacentHTML('beforeend', `
                      <div data-id="${activeCategory}" class="flip-card">
                          <div class="flip-card-front">
                              <img src="${imageSrc}"
                                  alt="graphic-design-illustration">
                          </div>
                          <div class="flip-card-back">
                              <svg xmlns="http://www.w3.org/2000/svg" width="88" height="43" viewBox="0 0 88 43"
                                  fill="none">
                                  <g clip-path="url(#clip0_2143_233)">
                                      <rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB" />
                                      <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z"
                                          fill="#1FDAB5" />
                                      <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M66.5973 1.99795C77.8653 1.99795 86.9999 10.9523 86.9999 21.9979C86.9999 33.0432 77.8653 41.9979 66.5973 41.9979C55.3292 41.9979 46.1946 33.0432 46.1946 21.9979C46.1946 10.9523 55.3292 1.99795 66.5973 1.99795Z"
                                          fill="#18CFAB" />
                                      <rect x="60" y="17" width="12" height="11" fill="white" />
                                  </g>
                                  <defs>
                                      <clipPath id="clip0_2143_233">
                                          <rect width="88" height="43" fill="white" />
                                      </clipPath>
                                  </defs>
                              </svg>
                              <p class="back-title text-green">Creative Design</p>
                              <p class="back-subtitle">Web Design</p>
                          </div>
                      </div>
            `);
    });
    totalImgLoaded += 3;
    imgCount += 3;
  }
};


// } else if(activeCategory !== "all") {
  //   imgDataBase[activeCategory].images.slice(imgCount, imgCount + 3);
  //   imgCount += 3;
  // }

categoriesList.addEventListener('click', (event) => {
  const target = event.target;

  if (target && target.classList.contains('work-category')) {

      categoryKey = target.dataset.tab;
      // categoryKey !== 'all' || 
      if (allImgLoaded === true) {
          loadMoreBtn.style.display = 'none';
      } else {
          loadMoreBtn.style.display = 'initial';
      }
      console.log(categoryKey);
      categoryFilter(categoryKey);
      setActiveTab(target);0
  }
});

const loader = document.querySelector(".loader");

loadMoreBtn.addEventListener('click', () => {
  const loadMoreChildren = loadMoreBtn.childNodes;

  loadMoreChildren.forEach((elem) => {
    elem.style.display = "none";
  })
  loader.style.display = "initial";
  
  setTimeout(() => {
    loader.style.display = "none";
    loadMoreChildren.forEach((elem) => {
      elem.style.display = "initial";
    })
      createCards();
  }, 2000);

});


// const loadMoreChildren = loadMoreBtn.childNodes;

//   loadMoreChildren.forEach((elem) => {
//     elem.style.display = "none";
//   })

//   loader.style.display = "initial";

//   setTimeout(() => {

//     loader.style.display = "none";
//     loadMoreChildren.forEach((elem) => {
//       elem.style.display = "initial";
//     })




// categoriesList.addEventListener("click", event => {
//   ourWorksContent.style.height = "618px";
//   loadMoreBtn.style.display = "";
//   categoryImg.forEach((elem) => {
//     elem.classList.add("hidden");
//     if ((elem.dataset.id === event.target.dataset.tab) && event.target.dataset.tab != "all") {
//       ourWorksContent.dataset.maxHeight = "1854";
//       console.log(ourWorksContent.dataset.maxHeight);
//       elem.classList.remove("hidden");
//     }
//   })
//   if (event.target.dataset.tab === "all") {
//     ourWorksContent.dataset.maxHeight = "7416";
//     categoryImg.forEach((elem) => {
//       elem.classList.remove("hidden");
//     })
//   }
// })


// let maxHeight = Number(ourWorksContent.dataset.maxHeight);
// let strHeight = (window.getComputedStyle(ourWorksContent).height).split("p");
// let ourWorksContentHeight = Number(strHeight[0]);
// const loader = document.querySelector(".loader");


// function showMore() {
//   const loadMoreChildren = loadMoreBtn.childNodes;

//   loadMoreChildren.forEach((elem) => {
//     elem.style.display = "none";
//   })

//   loader.style.display = "initial";

//   setTimeout(() => {

//     loader.style.display = "none";
//     loadMoreChildren.forEach((elem) => {
//       elem.style.display = "initial";
//     })

//     if (ourWorksContentHeight < maxHeight) {
//       ourWorksContent.style.height = ourWorksContentHeight + 618 + "px";
//     }

//     strHeight = (window.getComputedStyle(ourWorksContent).height).split("p");
//     ourWorksContentHeight = Number(strHeight[0]);
//     maxHeight = Number(ourWorksContent.dataset.maxHeight);

//     if (ourWorksContentHeight === maxHeight) {
//       loadMoreBtn.style.display = "none";
//     }

//   }, 2000)

// }

// loadMoreBtn.addEventListener("click", showMore)


const slider = document.querySelector(".slider-wrapper");
const sliderReviewers = document.querySelectorAll(".slider-item-photo");
const sliderReviewersWrap = document.querySelectorAll(".slider-item")
const reviewsContent = document.querySelectorAll(".review-wrapper");
let sliderActiveEl = document.querySelector(".slider-item-active");





slider.addEventListener("click", event => {

  if (event.target.parentElement.classList.contains("slider-item")) {
    sliderReviewersWrap.forEach((elem) => {
      elem.classList.remove("slider-item-active");
    })
    event.target.parentElement.classList.add("slider-item-active");
    reviewsContent.forEach((elem) => {
      elem.classList.add("hidden");

      if (elem.dataset.id === event.target.parentElement.dataset.tab) {
        elem.classList.remove("hidden");
      }

    })
  }


  if (event.target.closest("div").classList.contains("arrow-left")) {
    let activeSliderItem = document.querySelector(".slider-item-active");
    const lastSliderItem = document.querySelector("div[data-order='4']");

    if (activeSliderItem.dataset.order === "1") {
      activeSliderItem.classList.remove("slider-item-active");
      lastSliderItem.classList.add("slider-item-active");
      activeSliderItem = lastSliderItem;

      reviewsContent.forEach((elem) => {
        elem.classList.add("hidden");
        if (elem.dataset.id === activeSliderItem.dataset.tab) {
          elem.classList.remove("hidden");
        }
      })

      activeSliderItem = document.querySelector(".slider-item-active");

    } else if (activeSliderItem.dataset.order != "1") {

      activeSliderItem.classList.remove("slider-item-active");
      activeSliderItem = activeSliderItem.previousElementSibling;
      activeSliderItem.classList.add("slider-item-active");

      reviewsContent.forEach((elem) => {
        elem.classList.add("hidden");
        if (elem.dataset.id === activeSliderItem.dataset.tab) {
          elem.classList.remove("hidden");
        }
      })

      activeSliderItem = document.querySelector(".slider-item-active");

    }
  }


  if (event.target.closest("div").classList.contains("arrow-right")) {
    let activeSliderItem = document.querySelector(".slider-item-active");
    const firstSliderItem = document.querySelector("div[data-order='1']") //винести за івент

    if (activeSliderItem.dataset.order === "4") {
      activeSliderItem.classList.remove("slider-item-active");
      firstSliderItem.classList.add("slider-item-active");
      activeSliderItem = firstSliderItem;

      reviewsContent.forEach((elem) => {
        elem.classList.add("hidden");
        if (elem.dataset.id === activeSliderItem.dataset.tab) {
          elem.classList.remove("hidden");
        }
      })


    } else if (activeSliderItem.dataset.order != "4") {


      activeSliderItem.classList.remove("slider-item-active");
      activeSliderItem = activeSliderItem.nextElementSibling;
      activeSliderItem.classList.add("slider-item-active");

      reviewsContent.forEach((elem) => {
        elem.classList.add("hidden");
        if (elem.dataset.id === activeSliderItem.dataset.tab) {
          elem.classList.remove("hidden");
        }
      })

    }
  }
})


